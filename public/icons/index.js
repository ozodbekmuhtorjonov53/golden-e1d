import NavSolid from './Vector 14 (2).svg'
import HedSolid1 from './Vector 13 (1).svg'
import HedDashed from './Vector 11 (1).svg'
import HedSolid2 from './Vector 12.svg'
import HedTelefon from './Frame 11 (1).svg'
import svg1 from './Frame 8 (12).svg'
import svg2 from './Frame 8 (13).svg'
import svg3 from './Frame 8 (14).svg'
import svg4 from './Frame 8 (15).svg'
import svg5 from './Frame 8 (16).svg'
import svg6 from './Frame 8 (17).svg'
import svg7 from './Frame 9 (1).svg'
import svg8 from './Frame 8 (18).svg'

export {NavSolid}
export {HedSolid1}
export {HedDashed}
export {HedSolid2}
export {HedTelefon}
export {svg1}
export {svg2}
export {svg3}
export {svg4}
export {svg5}
export {svg6}
export {svg7}
export {svg8}