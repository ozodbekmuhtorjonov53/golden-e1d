import React from 'react'
// import { image8 } from '../../../public/image';
// import { svg23, svg24, svg25, svg26, svg27, svg29 } from '../../../public/icons';
// import Image from 'next/image';

const Heed7 = () => {
  return (
    <div className=" relative">
      <div className=" px-[24px] flex flex-wrap md:pl-[120px] md:pr-[130px] justify-between pt-[179px]">
        <div>
          <div className=" md:pl-5" data-aos="fade-up" data-aos-duration="1700">
            <p className=" text-[18px] text-[#20DB81] font-medium font-Monserat">
              DVIRs
            </p>
            <h1 className="text-[36px] md:text-[48px] text-[#162232] font-medium font-Inknut">
              Managing DVIRs
            </h1>
          </div>
          {/* page - 1 */}
          <div className=" flex flex-wrap gap-[150px] pt-10 md:pt-[63px]">
            <div
              className=" md:pl-5"
              data-aos="fade-down-right"
              data-aos-duration="1700"
            >
              {/* <Image src={svg23} alt="" /> */}
              <h1 className=" text-[18px] text-[#162232] pt-[10px] font-bold font-Monserat">
                Add DVIR
              </h1>
              <p className=" text-[16px] text-[#4D4B58] pt-3 font-Monserat font-medium w-[220px]">
                Tap on TODAY`s log to view & manage your current log.
              </p>
            </div>
            <div data-aos="fade-down-left" data-aos-duration="1700">
              {/* <Image src={svg24} alt="" /> */}
              <h1 className=" text-[18px] text-[#162232] pt-[10px] font-bold font-Monserat">
                Review last DVIR
              </h1>
              <p className=" text-[16px] text-[#4D4B58] pt-3 font-Monserat font-medium w-[260px]">
                Tap on a speciﬁc status in the events section to view location
                and notes.{" "}
              </p>
            </div>
          </div>
          {/* page - 2 */}
          <div className=" flex flex-wrap gap-[110px] pt-10 md:pt-[63px]">
            <div
              className=" md:pl-5"
              data-aos="fade-right"
              data-aos-duration="1700"
            >
              {/* <Image src={svg25} alt="" /> */}
              <h1 className=" text-[18px] text-[#162232] pt-[10px] font-bold font-Monserat">
                Defects
              </h1>
              <p className=" text-[16px] text-[#4D4B58] pt-3 font-Monserat font-medium w-[260px]">
                View past logs & violations if any. Tap on a log you would like
                to view or edit.
              </p>
            </div>
            <div data-aos="fade-left" data-aos-duration="1700">
              {/* <Image src={svg26} alt="" /> */}
              <h1 className=" text-[18px] text-[#162232] pt-[7px] font-bold font-Monserat">
                DVIRs History
              </h1>
              <p className=" text-[16px] text-[#4D4B58] pt-[8px] font-Monserat font-medium w-[220px]">
                Press on “Pencil” to edit or “+” to insert a past duty status.{" "}
              </p>
            </div>
          </div>
          {/* page - 3 */}
          <div className=" flex flex-wrap gap-[101px] pt-10 md:pt-[63px]">
            <div
              className=" md:pl-5"
              data-aos="fade-up-right"
              data-aos-duration="1700"
            >
              {/* <Image src={svg27} alt="" /> */}
              <h1 className=" text-[18px] text-[#162232] pt-[10px] font-bold font-Monserat">
                Correct Defects
              </h1>
              <p className=" text-[16px] text-[#4D4B58] pt-3 font-Monserat font-medium w-[270px]">
                Similar to paper logs, view your hours or service on the graph
                grid.
              </p>
            </div>
            <div data-aos="fade-up-left" data-aos-duration="1700">
              {/* <Image src={svg29} alt="svg29" /> */}
              <h1 className=" text-[18px] text-[#162232] pt-[10px] font-bold font-Monserat">
                Edit DVIR
              </h1>
              <p className=" text-[16px] text-[#4D4B58] pt-3 font-Monserat font-medium w-[230px]">
                Tap “Certify” and sign your log when your shift ends.
              </p>
            </div>
          </div>
        </div>
        <div
          className=" z-10 md:pr-[80px]"
          data-aos="zoom-in"
          data-aos-duration="1700"
        >
          {/* <Image
            className=" sm:w-full mt-[60px] bg-cover border-solid border-2 border-gray-400 md:mt-0 rounded-[30px]"
            src={image8}
            alt=""
          /> */}
        </div>
      </div>
    </div>
  );
}

export default Heed7