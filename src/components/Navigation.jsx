"use client";

import Link from "next/link";
import { useEffect, useState } from "react";
// import { Logo, logo, vector } from "../../public/image";
import Image from "next/image";
// import { solid1 } from "../../public/icons";
import AOS from "aos";
import "aos/dist/aos.css";
import { Logo1, NavKorsat } from "../../public/image";
import { NavSolid } from "../../public/icons";


const NavBar = () => {
  const [isOpen, setIsOpen] = useState(false);
  useEffect(() => {
    AOS.init({});
  });

  return (
    <div className="bg-1 container mx-auto max-w-screen-2xl px-6 md:px-[120px]">
      <div className="flex items-center justify-between py-4">
        <div data-aos="fade-right" data-aos-duration="1500">
          <Image className=" bg-cover cursor-pointer w-[152px] md:w-[195px] md:h-[34px]" src={Logo1} alt="logo" />
        </div>
        <ul className="hidden md:flex gap-12 font-Monserat font-medium text-white text-[16px]">
          <li
            className=" hover:text-[#6D25E1]"
            data-aos="fade-down"
            data-aos-duration="1000"
          >
            <Link href="/">Features</Link>
          </li>
          <li
            className="hover:text-[#6D25E1]"
            data-aos="fade-down"
            data-aos-duration="1200"
          >
            <Link href="/devices">Devices</Link>
          </li>
          <li
            className="hover:text-[#6D25E1]"
            data-aos="fade-down"
            data-aos-duration="1300"
          >
            <Link href="/pricing">Pricing</Link>
          </li>
          <li
            className=" hover:text-[#6D25E1]"
            data-aos="fade-down"
            data-aos-duration="1500"
          >
            <Link href="#">Contact Us</Link>
          </li>
        </ul>
        <button
          className="hidden md:flex bg-[#6D25E1] text-white font-Monserat font-medium py-2 text-[16px] px-8 gap-2 rounded-full items-center"
          data-aos="fade-left"
          data-aos-duration="1500"
        >
          Sign in
          <Image src={NavKorsat} alt="vector" />
        </button>
        {/* Hamburger menu */}
        <div
          type="button"
          className=" sm:hidden inline-flex items-center justify-center text-white focus:outline-none  focus:ring-inset focus:ring-white"
          aria-controls="mobile-menu"
          aria-expanded="false"
          onClick={() => setIsOpen(!isOpen)}
        >
          <button className={`${isOpen ? "hidden" : "block"} h-9 w-9`}>
            <div
              className="h-1 w-[28px] bg-white rounded"
              data-aos="fade-down"
              data-aos-duration="900"
            ></div>
            <div
              className="h-1 w-[28px] bg-white mt-[5px] rounded"
              data-aos="fade-down"
              data-aos-duration="1100"
            ></div>
            <div
              className="h-1 w-[28px] bg-white mt-[5px] rounded"
              data-aos="fade-down"
              data-aos-duration="1300"
            ></div>
          </button>
          <svg
            className={`${isOpen ? "block" : "hidden"} h-9 w-9 text-white`}
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            aria-hidden="true"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M6 18L18 6M6 6l12 12"
            />
          </svg>
        </div>
      </div>
      <div>
        <Image className=" bg-cover w-full" src={NavSolid} alt="solid1" />
      </div>
      {/* Mobile menu */}
      <div className=" mt-[12px] absolute z-10 bg-[#ffffff]  w-full left-0">
        <div
          className={`${isOpen ? "block" : "hidden"} sm:hidden`}
          id="mobile-menu"
        >
          <div className="px-2 pt-2 pb-3 space-y-1">
            <div class="border-dashed border-2 border-[#000000] mx-[20px] opacity-10 "></div>
            <li className=" text-[19px] uppercase my-2 mx-[1px] px-4 py-2 text-black hover:bg-slate-600 rounded font-Monserat font-medium  list-none">
              <Link href={"/"}>Features</Link>
            </li>
            <div class="border-dashed border-2 border-[#000000] mx-[20px] opacity-10 "></div>
            <li className=" text-[19px] uppercase my-2 mx-[1px] px-4 py-2 text-black hover:bg-slate-600 rounded font-Monserat font-medium  list-none">
              <Link href={"/devices"}>Devices</Link>
            </li>
            <div class="border-dashed border-2 border-[#000000] mx-[20px] opacity-10 "></div>

            <li className=" text-[19px] uppercase my-2 mx-[1px] px-4 py-2 text-black hover:bg-slate-600 rounded font-Monserat font-medium  list-none">
              <Link href={"/pricing"}>Pricing</Link>
            </li>
            <div class="border-dashed border-2 border-[#000000] mx-[20px] opacity-10 "></div>
            <li className=" text-[19px] uppercase my-2 mx-[1px] px-4 py-2 text-black hover:bg-slate-600 rounded font-Monserat font-medium  list-none">
              <Link href={"/pricing"}>Contact Us</Link>
            </li>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NavBar;
