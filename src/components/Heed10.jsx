import React from "react";
// import { image11 } from "../../../public/image";
// import { svg13 } from "../../../public/icons";
// import Image from "next/image";

const Heed10 = () => {
  return (
    <div className="flex flex-wrap relative justify-between pl-[200px] pr-[107px] pt-[140px]">
      <div data-aos="zoom-in" data-aos-duration="1700">
        {/* <Image
          className="wedu pt-[30px] rounded-[25px] bg-cover relative z-10"
          src={image11}
          alt=""
        /> */}
      </div>
      <div>
        <div className=" pl-[10px]" data-aos="fade-up" data-aos-duration="1700">
          <p className=" font-Monserat text-[18px] font-bold text-[#20DB81]">
            App review
          </p>
          <h1 className=" font-Inknut font-medium text-[48px] text-[#162232]">
            Inspection Mode
          </h1>
        </div>
        {/* page boshi */}
        <div className="z-10 relative bg-[#F9F9FC] border-[1px] border-solid border-gray-400 px-10 py-7 mt-[60px]">
          <div
            className=" flex items-start gap-[18px]"
            data-aos="fade-right"
            data-aos-duration="1700"
          >
            {/* <Image className=" pt-[5px]" src={svg13} alt="" /> */}
            <p className=" font-Monserat font-medium text-[17px] text-[#4D4B58] w-[534px]">
              Ta``DOT Inspection in the menu & press ``Start Inspectio``. Let an
              officer to view your logs directly from your mobile device. Show
              this instruction card if requested.
            </p>
          </div>
          <div className="w-full left-0 mt-7 absolute h-[1px] bg-black opacity-[0.2]"></div>
          <div
            className=" flex pt-[56px] items-center gap-[18px]"
            data-aos="fade-left"
            data-aos-duration="1700"
          >
            {/* <Image className="" src={svg13} alt="" /> */}
            <p className=" font-Monserat font-medium text-[17px] text-[#4D4B58] w-[534px]">
              An inspector may press arrows to view previous or next day`s logs.
            </p>
          </div>
          <div className="w-full left-0 mt-7 absolute h-[1px] bg-black opacity-[0.2]"></div>
          <div
            className=" flex pt-[56px] items-start gap-[18px]"
            data-aos="fade-right"
            data-aos-duration="1700"
          >
            {/* <Image className=" pt-[5px]" src={svg13} alt="" /> */}
            <p className=" font-Monserat font-medium text-[17px] text-[#4D4B58] w-[534px]">
              An inspector may view the log form, the log graph and the log
              events with notes.
            </p>
          </div>
          <div className="w-full left-0 mt-7 absolute h-[1px] bg-black opacity-[0.2]"></div>
          <div
            className=" flex pt-[48px] items-start gap-[18px]"
            data-aos="fade-left"
            data-aos-duration="1700"
          >
            {/* <Image className=" pt-[5px]" src={svg13} alt="" /> */}
            <p className=" font-Monserat font-medium text-[17px] text-[#4D4B58] w-[534px]">
              Exit the inspection mode by pressing back arrow in the left top
              corner of the app.
            </p>
          </div>
        </div>
        {/* page oxiri */}
        <div className=" pt-[48px] pl-5">
          <h1
            className=" font-Inknut font-medium text-[32px] text-[#162232]"
            data-aos="fade-right"
            data-aos-duration="1700"
          >
            Sending Logs
          </h1>
          <p
            className="pt-4 w-[550px] font-Monserat font-medium text-[16px] text-[ #4D4B58]"
            data-aos="fade-left"
            data-aos-duration="1700"
          >
            GoldenELD is capable of producing and transferring the ELD records
            via telematic s transfer methods: Wireless Web services and
            Email.♦In order to send the ELD records via Web services driver must
            press <span className=" text-[#20DB81]">`DOT Inspection`</span> menu
            item and then press
            <span className=" text-[#20DB81]">`Send Logs`</span> button. In
            order to send the ELD records via Email a driver must press{" "}
            <span className=" text-[#20DB81]">`DOT Inspection`</span> menu item,
            press <span className=" text-[#20DB81]">`Email Logs`</span>, enter
            an email provided by an authorized afetyofficial and press{" "}
            <span className=" text-[#20DB81]">`Send`</span> button.
          </p>
        </div>
      </div>
    </div>
  );
};

export default Heed10;
