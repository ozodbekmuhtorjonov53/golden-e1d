import React from 'react'
// import { svg13, svg14, svg15, svg16 } from '../../../public/icons';
// import { image3 } from '../../../public/image';
// import Image from 'next/image';

const Heed4 = () => {
  return (
    <div className="pt-[140px] z-10 pb-[140px] absolute md:px-[100px]">
      <div className=" flex flex-wrap gap-[160px]">
        <div>
          <div>
            <p
              className="pl-5 text-[18px] text-[#20DB81] font-medium font-Monserat"
              data-aos="fade-up"
              data-aos-duration="1700"
            >
              HOS Rules
            </p>
            <h1
              className="pl-5 pt-4 text-[32px] md:text-[48px] text-[#162232] font-Monserat font-bold"
              data-aos="fade-up"
              data-aos-duration="1700"
            >
              Driving with ELD
            </h1>
          </div>
          <div className=" bg-[#F9F9FC] w-[390px] md:w-[657px] border-gray-300 border mt-[60px]">
            <div
              className=" flex px-[24px] items-center gap-4 md:px-10"
              data-aos="fade-right"
              data-aos-duration="1700"
            >
              {/* <Image className=" w-[25px]" src={svg13} alt="" /> */}
              <p className=" tetx-[16px] text-[#4D4B58] py-7 font-medium font-Monserat">
                Once ELD connected, your driving time will be captured
                automatically
              </p>
            </div>
            <div className=" border-gray-300 border"></div>
            <div
              className=" flex px-[24px] items-center gap-4 md:px-10"
              data-aos="fade-left"
              data-aos-duration="1700"
            >
              {/* <Image className=" w-[30px]" src={svg13} alt="" /> */}
              <p className=" tetx-[16px] text-[#4D4B58] py-7 font-medium font-Monserat">
                Once your vehicle is moving at 5 mph or more, your duty status
                is switched to Driving
              </p>
            </div>
            <div className=" border-gray-300 border"></div>
            <div
              className=" flex px-[24px] items-center gap-4 md:px-10"
              data-aos="fade-right"
              data-aos-duration="1700"
            >
              {/* <Image className=" w-[30px]" src={svg14} alt="" /> */}
              <p className=" tetx-[16px] text-[#4D4B58] py-7 font-medium font-Monserat">
                Your logs and others features are not available while in Driving
                mode due to safety reasons
              </p>
            </div>
            <div className=" border-gray-300 border"></div>
            <div
              className=" flex px-[24px] items-center gap-4 md:px-10"
              data-aos="fade-left"
              data-aos-duration="1700"
            >
              {/* <Image className=" w-[70px]" src={svg13} alt="" /> */}
              <p className=" tetx-[16px] text-[#4D4B58] py-7 font-medium font-Monserat">
                Once your vehicle is stopped, you may change your duty status by
                tapping on the status circle. The app will remind you to make a
                selection in 5 minutes. If no selectio is made, your duty status
                will be switched to On Duty.
              </p>
            </div>
          </div>
          {/* 2 ta pagee boshlanishi */}
          <div
            className="flex flex-wrap justify-between px-7 pt-[67px]"
            data-aos="fade-up"
            data-aos-duration="1800"
          >
            <div>
              {/* <Image src={svg15} alt="" /> */}
              <h1 className=" text-[18px] text-[#162232] font-Monserat font-medium pt-[16px]">
                Hours
              </h1>
              <p className=" text-[16px] text-[#4D4B58] leading-[24px] font-Monserat font-medium pt-2 md:w-[270px] w-[342px]">
                Available driving hours, required breaks, on-duty limits and
                required off-duty time are calculated automatically
              </p>
            </div>
            <div className="pt-10 md:pt-0 relative">
              {/* <Image src={svg16} alt="" /> */}
              <h1 className=" text-[18px] text-[#162232] font-Monserat font-medium pt-[22px]">
                Warnings
              </h1>
              <p className=" text-[16px] text-[#4D4B58] leading-[24px] font-Monserat font-medium pt-3 md:w-[265px] w-[342px] ">
                Visual notiﬁcations and sound warnings help you to avoid hours
                of service violations and stay compliant
              </p>
            </div>
          </div>
        </div>
        <div
          className="bg-cover pl-1 md:pl-0"
          data-aos="zoom-in"
          data-aos-duration="1700"
        >
          {/* <Image
            className="w-[360px] pl-[20px] bg-cover rounded-[40px]"
            src={image3}
            alt=""
          /> */}
        </div>
      </div>
    </div>
  );
}

export default Heed4