import React from "react";
import Image from "next/image";
import { svg1, svg2, svg3, svg4, svg5, svg6, svg7, svg8 } from "../../public/icons";

export default function Heed2() {
  return (
    <div className="px-[24px] bg-[#FCFCFC] md:px-[120px] pb-[140px]">
      <div className=" pt-[50px]">
        <div className=" pt-[140px]">
          <p
            className=" text-[18px] text-[#20DB81] font-Monserat font-medium"
            data-aos="fade-up"
            data-aos-duration="1700"
          >
            ELD Compliance
          </p>
          <h1
            className="text-[36px] pt-2 md:text-[48px] text-[#162232] font-Inknut font-medium"
            data-aos="fade-up"
            data-aos-duration="1700"
          >
            Main Features
          </h1>
        </div>
        {/* card golden pade -1 */}
        <div className=" flex flex-wrap justify-between">
          {/* card - 1 */}
          <div
            className="pt-10 md:pt-20"
            data-aos="fade-right"
            data-aos-duration="1700"
          >
            <Image src={svg1} alt="svg1" />
            <h1 className=" text-[18px] text-[#162232] font-bold pt-[10px] font-Monserat">
              Automatic HOS
            </h1>
            <p
              className=" text-[16px] font-Monserat font-medium pt-4 text-[#4D4B58] 
            w-[332px] md:w-[250px] leading-[30px]"
            >
              Automatic hours of service calculation and violation alerts.
              Automatic recording of driving time, miles and locations.
            </p>
          </div>
          {/* card - 2 */}
          <div
            className="pt-10 md:pt-20"
            data-aos="fade-down-right"
            data-aos-duration="1700"
          >
            <Image src={svg2} alt="svg2" />
            <h1 className=" text-[18px] text-[#162232] font-bold pt-[14px] font-Monserat">
              DOT Inspection Mode
            </h1>
            <p
              className=" text-[16px] font-Monserat font-medium pt-4 text-[#4D4B58] 
            w-[332px] md:w-[270px] leading-[30px]"
            >
              Simply show logs on your phone or tablet. No printer needed.
            </p>
          </div>
          {/* card - 3 */}
          <div
            className="pt-10 md:pt-20"
            data-aos="fade-down-left"
            data-aos-duration="1700"
          >
            <Image src={svg3} alt="svg3" />
            <h1 className=" text-[18px] text-[#162232] font-bold pt-[10px] font-Monserat">
              Multiple HOS Rules
            </h1>
            <p className=" text-[16px] font-Monserat font-medium pt-4 text-[#4D4B58] w-[332px] md:w-[270px] leading-[30px]">
              Compliant with multiple HOS rules including Property/Passenger
              60-hour/7-day & 70-hour/8-day
            </p>
          </div>
          {/* card - 4 */}
          <div
            className=" pt-10 md:pt-20"
            data-aos="fade-left"
            data-aos-duration="1700"
          >
            <Image src={svg4} alt="svg4" />
            <h1 className=" text-[18px] text-[#162232] font-bold pt-[10px] font-Monserat">
              Electronic DVIR
            </h1>
            <p className=" text-[16px] font-Monserat font-medium pt-4 text-[#4D4B58] w-[332px] md:w-[270px] leading-[30px]">
              Vehicle inspection reports are created and submitted in seconds
            </p>
          </div>
        </div>
        {/* card golden pade - 2 */}
        <div className=" flex flex-wrap justify-between pt-1">
          {/* card - 1 */}
          <div
            className=" pt-10 md:pt-20"
            data-aos="fade-right"
            data-aos-duration="1700"
          >
            <Image src={svg5} alt="svg5" />
            <h1 className=" text-[18px] text-[#162232] font-bold pt-[10px] font-Monserat">
              Compliance Monitoring
            </h1>
            <p className=" text-[16px] font-Monserat font-medium pt-4 text-[#4D4B58] w-[332px] md:w-[270px] leading-[30px]">
              Monitor your drivers hours of service logs and DVIRs. Receive
              alerts to prevent violations.
            </p>
          </div>
          {/* card - 2 */}
          <div
            className="pt-10 md:pt-[75px]"
            data-aos="fade-up-right"
            data-aos-duration="1700"
          >
            <Image src={svg6} alt="svg6" />
            <h1 className=" text-[18px] md:pt-4 text-[#162232] font-bold pt-[5px] font-Monserat">
              Fleet Tracking
            </h1>
            <p className=" text-[16px] font-Monserat font-medium pt-4 text-[#4D4B58] w-[332px] md:w-[270px] leading-[30px]">
              Track your vehicles in real time and view their location history.
            </p>
          </div>
          {/* card - 3 */}
          <div
            className=" pt-10 md:pt-20"
            data-aos="fade-up-left"
            data-aos-duration="1700"
          >
            <Image src={svg7} alt="svg7" />
            <h1 className=" text-[18px] text-[#162232] font-bold pt-[10px] font-Monserat">
              IFTA reporting
            </h1>
            <p className=" text-[16px] font-Monserat font-medium pt-4 text-[#4D4B58] w-[332px] md:w-[270px] leading-[30px]">
              Automatic IFTA state mileage reporting saves you time and money.
            </p>
          </div>
          {/* card - 4 */}
          <div
            className="pt-10 md:pt-[75px]"
            data-aos="fade-left"
            data-aos-duration="1700"
          >
            <Image src={svg8} alt="svg8" />
            <h1 className=" text-[18px] text-[#162232] font-bold pt-3 font-Monserat">
              Access Permissions
            </h1>
            <p className=" text-[16px] font-Monserat font-medium pt-4 text-[#4D4B58] w-[332px] md:w-[270px] leading-[30px]">
              Manage permissions for ﬂeet managers, compliance ofﬁcers, drivers,
              accountants, brokers and customers
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
