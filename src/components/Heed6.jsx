import React from "react";
// import { image7 } from "../../../public/image";
// import {
//   svg17,
//   svg18,
//   svg19,
//   svg20,
//   svg21,
//   svg22,
// } from "../../../public/icons";
import Solid02 from "./solid02";
// import Image from "next/image";

const Heed6 = () => {
  return (
    <div className=" relative">
      <div className=" px-[24px] flex flex-wrap-reverse md:pl-[200px] md:pr-[130px] justify-between pt-[179px]">
        <div className=" z-10" data-aos="zoom-in" data-aos-duration="1700">
          {/* <Image
            className="mt-[60px] md:mt-0 rounded-[30px]"
            src={image7}
            alt=""
          /> */}
        </div>
        <div>
          <div className=" md:pl-5" data-aos="fade-up" data-aos-duration="1700">
            <p className=" text-[18px] text-[#20DB81] font-medium font-Monserat">
              Logs
            </p>
            <h1 className="text-[36px] md:text-[48px] text-[#162232] font-medium font-Inknut">
              Managing Logs
            </h1>
          </div>
          {/* page - 1 */}
          <div className=" flex flex-wrap gap-[150px] pt-10 md:pt-[63px]">
            <div
              className=" md:pl-5"
              data-aos="fade-down-right"
              data-aos-duration="1700"
            >
              {/* <Image src={svg17} alt="" /> */}
              <h1 className=" text-[18px] text-[#162232] pt-[10px] font-bold font-Monserat">
                Today`s Log
              </h1>
              <p className=" text-[16px] text-[#4D4B58] pt-3 font-Monserat font-medium w-[220px]">
                Tap on TODAY`s log to view & manage your current log.
              </p>
            </div>
            <div data-aos="fade-down-left" data-aos-duration="1700">
              {/* <Image src={svg18} alt="" /> */}
              <h1 className=" text-[18px] text-[#162232] pt-[10px] font-bold font-Monserat">
                Statuses/Events
              </h1>
              <p className=" text-[16px] text-[#4D4B58] pt-3 font-Monserat font-medium w-[260px]">
                Tap on a speciﬁc status in the events section to view location
                and notes.{" "}
              </p>
            </div>
          </div>
          {/* page - 2 */}
          <div className=" flex flex-wrap gap-[110px] pt-10 md:pt-[63px]">
            <div
              className=" md:pl-5"
              data-aos="fade-right"
              data-aos-duration="1700"
            >
              {/* <Image src={svg19} alt="" /> */}
              <h1 className=" text-[18px] text-[#162232] pt-[10px] font-bold font-Monserat">
                Logs History
              </h1>
              <p className=" text-[16px] text-[#4D4B58] pt-3 font-Monserat font-medium w-[260px]">
                View past logs & violations if any. Tap on a log you would like
                to view or edit.
              </p>
            </div>
            <div data-aos="fade-left" data-aos-duration="1700">
              {/* <Image src={svg20} alt="" /> */}
              <h1 className=" text-[18px] text-[#162232] pt-[10px] font-bold font-Monserat">
                Edit/Insert Status
              </h1>
              <p className=" text-[16px] text-[#4D4B58] pt-3 font-Monserat font-medium w-[220px]">
                Press on “Pencil” to edit or “+” to insert a past duty status.{" "}
              </p>
            </div>
          </div>
          {/* page - 3 */}
          <div className=" flex flex-wrap gap-[101px] pt-10 md:pt-[63px]">
            <div
              className=" md:pl-5"
              data-aos="fade-up-right"
              data-aos-duration="1700"
            >
              {/* <Image src={svg21} alt="" /> */}
              <h1 className=" text-[18px] text-[#162232] pt-[10px] font-bold font-Monserat">
                Graph Grid
              </h1>
              <p className=" text-[16px] text-[#4D4B58] pt-3 font-Monserat font-medium w-[270px]">
                Similar to paper logs, view your hours or service on the graph
                grid.
              </p>
            </div>
            <div data-aos="fade-up-left" data-aos-duration="1700">
              {/* <Image src={svg22} alt="" /> */}
              <h1 className=" text-[18px] text-[#162232] pt-[10px] font-bold font-Monserat">
                Certify Log
              </h1>
              <p className=" text-[16px] text-[#4D4B58] pt-3 font-Monserat font-medium w-[230px]">
                Tap “Certify” and sign your log when your shift ends.
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="">
        <Solid02 />
      </div>
    </div>
  );
};

export default Heed6;
