import React from "react";
// import { svg33, svg34, svg35, svg36 } from "../../../public/icons";
// import { image12, image13 } from "../../../public/image";
// import Image from "next/image";

const Heed11 = () => {
  return (
    <div className=" flex flex-wrap-reverse justify-between px-[24px] md:pl-[120px] md:pr-[170px] mt-[160px] pb-[147px]">
      <div>
        <div className=" pt-[]" data-aos="fade-up" data-aos-duration="1700">
          <p className=" font-Monserat font-bold text-[18px] text-[#20DB81]">
            Devices
          </p>
          <h1 className=" font-Inknut font-medium w-[600px] pt-[10px] text-[32px] md:text-[48px] text-[#162232]">
            GoldenELD Malfunction Manual
          </h1>
          <p className="hidden md:font-Monserat font-medium pt-7 text-[16px] text-[#4D4B58]">
            In accordance with the guidelines set forth in{" "}
            <span className=" text-[#20DB81]">395.34</span>
          </p>
        </div>
        {/* CARD 1 - MI BU ENDI */}
        <div
          className=" pt-[63px]"
          data-aos="fade-right"
          data-aos-duration="1700"
        >
          {/* <Image src={svg33} alt="" /> */}
          <h1 className=" font-Monserat text-[18px] text-[#162232] font-medium pt-[13px]">
            Malfunction indication
          </h1>
          <p className="pt-[12px] font-Monserat font-medium w-[342px] md:w-[460px] text-[16px] text-[#4D4B58]">
            Immediately contact the support if LED light on the device is off
            when the device is plugged into the diagnostic port or if the
            malfunction reported by the app.
          </p>
        </div>
        {/* CARD 2 - MI BU ENDI */}
        <div
          className=" pt-[35px]"
          data-aos="fade-left"
          data-aos-duration="1700"
        >
          {/* <Image src={svg34} alt="" /> */}
          <h1 className=" font-Monserat text-[18px] text-[#162232] font-medium pt-[13px]">
            Note the malfunction
          </h1>
          <p className="pt-[12px] font-Monserat font-medium w-[342px] md:w-[460px] text-[16px] text-[#4D4B58]">
            Note the malfunction and provide a written notice to your fleet
            within 24 hours.
          </p>
        </div>
        {/* CARD 3 - MI BU ENDI */}
        <div
          className=" pt-[35px]"
          data-aos="fade-right"
          data-aos-duration="1700"
        >
          {/* <Image src={svg35} alt="" /> */}
          <h1 className=" font-Monserat text-[18px] text-[#162232] font-medium pt-[13px]">
            Switch to paper logs
          </h1>
          <p className="pt-[12px] font-Monserat font-medium w-[342px] md:w-[460px] text-[16px] text-[#4D4B58]">
            Keep a paper log for that day and until the device is repaired or
            replaced. In the event of an inspection, display the previous 7 days
            from the app.
          </p>
        </div>
        {/* CARD 4 - MI BU ENDI */}
        <div
          className=" pt-[35px]"
          data-aos="fade-left"
          data-aos-duration="1700"
        >
          {/* <Image src={svg36} alt="" /> */}
          <h1 className=" font-Monserat text-[18px] text-[#162232] font-medium pt-[13px]">
            8 days rule
          </h1>
          <p className="pt-[12px] font-Monserat font-medium w-[342px] md:w-[460px] text-[16px] text-[#4D4B58]">
            In the event of an ELD malfunction, the motor carrier must take
            actions to correct the malfunction within 8 days of discovery..
          </p>
        </div>
      </div>
      {/* rasmlarni boshlanishi wekilu */}
      <div
        className=" relative z-10 pt-[40px]"
        data-aos="zoom-in"
        data-aos-duration="1700"
      >
        {/* <Image className=" bg-cover pl-[50px]" src={image12} alt="" /> */}
        {/* <Image className="mt-[60px] bg-cover" src={image13} alt="" /> */}
      </div>
    </div>
  );
};

export default Heed11;
