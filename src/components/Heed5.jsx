import React from "react";
// import { image4, image5, image6 } from "../../../public/image";
// import Image from "next/image";

const Heed5 = () => {
  return (
    <div className=" relative w-full pt-[2250px] md:pt-[1190px]">
      <div className=" bg-[#F9F9FC] w-full pt-[60px] pb-[72px] px-[24px] md:px-[120px]">
        <h1
          className="text-[32px] md:text-[48px] text-[#162232] font-Inknut"
          data-aos="fade-up"
          data-aos-duration="1700"
        >
          Duty Status
        </h1>
        <p
          className="text-[16px] text-[#4D4B58] font-medium font-Monserat md:w-[730px] pt-[60px]"
          data-aos="fade-left"
          data-aos-duration="1700"
        >
          Set duty status with just two-clicks. Status is updated automatically
          when driving starts or stops, Easy-to-use interface allows drivers to
          spend less time doing paperwork and more time driving. ELD simpliﬁes
          training and prevents log errors.
        </p>
        <div className=" flex flex-wrap gap-[60px]">
          {/* page - 1 */}
          <div
            className="pt-[39px]"
            data-aos="fade-right"
            data-aos-duration="1700"
          >
            <h1 className=" text-[18px] text-[#162232] font-medium font-Monserat">
              Current Status
            </h1>
            <p className=" pt-3 w-[260px] text-[16px] text-[#4D4B58] font-Monserat font-medium">
              Current duty status is always displayed on Status page inside of
              status circle along with available or reset hours. Current Status
            </p>
            {/* <Image className=" mt-[48px]" src={image4} alt="" /> */}
          </div>
          {/* page - 2 */}
          <div
            className="pt-[39px]"
            data-aos="fade-up"
            data-aos-duration="1700"
          >
            <h1 className=" text-[18px] text-[#162232] font-medium font-Monserat">
              Current Status
            </h1>
            <p className=" pt-3 w-[260px] text-[16px] text-[#4D4B58] font-Monserat font-medium">
              Current duty status is always displayed on Status page inside of
              status circle along with available or reset hours. Current Status
            </p>
            {/* <Image className=" mt-[48px]" src={image5} alt="" /> */}
          </div>
          {/* page - 3 */}
          <div
            className="pt-[39px]"
            data-aos="fade-left"
            data-aos-duration="1700"
          >
            <h1 className=" text-[18px] text-[#162232] font-medium font-Monserat">
              Current Status
            </h1>
            <p className=" pt-3 w-[260px] text-[16px] text-[#4D4B58] font-Monserat font-medium">
              Current duty status is always displayed on Status page inside of
              status circle along with available or reset hours. Current Status
            </p>
            {/* <Image className=" mt-[48px]" src={image6} alt="" /> */}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Heed5;
