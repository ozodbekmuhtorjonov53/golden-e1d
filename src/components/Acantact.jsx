"use client";

import React, { useState, useEffect } from "react";
// import { app, hdlog, icon1, play } from "../../public/image";
// import { soat, tel, telgrm, instg, lac, mail, fzbuk } from "../../public/icons";
// import Image from "next/image";
import AOS from "aos";
import "aos/dist/aos.css";

export default function Acantact() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [subject, setSubject] = useState("");
  const [phone, setPhone] = useState("");
  const [message, setMessage] = useState("");
  let url = `https://api.telegram.org/bot6014442812:AAGXz-Vxb1vo6J6P4jwh6SAn84jCYWj-1m0/sendMessage?chat_id=491149912&text=
  -Isim: ${name},
  -Email:${email},
   -Subject: ${subject},
    -Phone:${phone},
     -Message:${message}`;

  function sendMessageFunction() {
    fetch(url)
      .then((res) => res.json())
      .then((json) => console.log(json));
  }

  useEffect(() => {
    AOS.init({});
  });
  return (
    <div className="container px-40 mx-auto bg-[#1A1524]">
      <div className=" pl-[20px] border">
        <h1
          className="text-5xl mt-[80px] text-white"
          data-aos="fade-up"
          data-aos-duration="1700"
        >
          Contact Us
        </h1>
        <br></br>
        <div className="input-igonka flex mt-[80px] ">
          <div className="cantac-imput-chap md: selection:flex-wrap-wrap ">
            <form onChange={sendMessageFunction}>
              <div className="inputlar1 flex">
                <div
                  className=""
                  data-aos="fade-down-right"
                  data-aos-duration="1700"
                >
                  <label className=" text-white" htmlFor=" name ">
                    Your name
                  </label>
                  <br />
                  <input
                    onChange={(e) => setName(e.target.value)}
                    className=" w-[285px] h-[44px] rounded-[40px] bg-[#858487] cursor-pointer mt-[12px]"
                    type="text"
                    id="name"
                    placeholder="   Your name"
                  />
                </div>
                <div
                  className=" pl-[128px]"
                  data-aos="fade-down-left"
                  data-aos-duration="1700"
                >
                  <label className=" text-white" htmlFor="email">
                    E-mail *
                  </label>
                  <br />
                  <input
                    onChange={(e) => setEmail(e.target.value)}
                    className=" w-[285px] h-[44px] rounded-[40px] bg-[#858487] cursor-pointer mt-[12px]"
                    type="Email"
                    id="email"
                    placeholder="   example@mail.com"
                  />
                </div>
              </div>
              <div className="inputlar2 flex mt-[40px]">
                <div
                  className=""
                  data-aos="fade-up-right"
                  data-aos-duration="1700"
                >
                  <label className=" text-white" htmlFor="subject ">
                    Subject
                  </label>
                  <br />
                  <input
                    onChange={(e) => setSubject(e.target.value)}
                    className=" w-[285px] h-[44px] rounded-[40px] bg-[#858487] cursor-pointer mt-[12px]"
                    type="text"
                    id="subject"
                    placeholder="   A Subject"
                  />
                </div>
                <div
                  className=" pl-[128px]"
                  data-aos="fade-up-left"
                  data-aos-duration="1700"
                >
                  <label className=" text-white" htmlFor="phone ">
                    Phone Number
                  </label>
                  <br />
                  <input
                    onChange={(e) => setPhone(e.target.value)}
                    className=" w-[285px] h-[44px] rounded-[40px] bg-[#858487] cursor-pointer mt-[12px]"
                    type="Number"
                    id="phone"
                    placeholder="   Your Number"
                  />
                </div>
              </div>
              <div
                className="input-text mt-[40px]"
                data-aos="zoom-in"
                data-aos-duration="1700"
              >
                <label className=" text-white" htmlFor="message">
                  Message
                </label>
                <br />
                <input
                  onChange={(e) => setMessage(e.target.value)}
                  className=" pb-[178px] pr-[520px] bg-[#858487] mt-[12px] cursor-pointer rounded-[8px]"
                  type="text"
                  id="message"
                  placeholder="   Your Message ..."
                />
              </div>

              <button
                onClick={url}
                type="submit"
                className=" w-[126px] h-[45px] bg-[#6D25E1] text-white mt-[40px] rounded-[40px] flex
                  pl-[22px] pt-[9px]"
              >
                Send
                {/* <Image src={icon1} alt="#-btn" className=" pt-[8px] ml-[8px]" /> */}
              </button>
            </form>
          </div>

          <div className="cantac-imput-ong ml-[145px]">
            <div className="a" data-aos="fade-right" data-aos-duration="1700">
              <div className=" flex items-center mt-[5px]">
                <div>{/* <Image src={lac} alt="lacatsa" /> */}</div>
                <div className=" text-white ml-[10px]">
                  <h1>Our Location</h1>
                </div>
              </div>
              <p className="text-white ml-[28px] mt-[14px]">
                2115 FRONT ST STE D <br /> CUYAHOGA FLS, OH 44221
              </p>
            </div>
            <div className="b" data-aos="fade-left" data-aos-duration="1700">
              <div className=" flex items-center mt-[51px]">
                <div>{/* <Image src={tel} alt="lacatsa" /> */}</div>
                <div className=" text-white ml-[10px]">
                  <h1>Call Us Now</h1>
                </div>
              </div>
              <p className=" text-white mt-[14px]">+1 (646) 477-7070 </p>
            </div>
            <div className="c" data-aos="fade-right" data-aos-duration="1700">
              <div className=" flex items-center mt-[51px]">
                <div>{/* <Image src={soat} alt="lacatsa" /> */}</div>
                <div className=" text-white ml-[10px]">
                  <h1>Opening Hours</h1>
                </div>
              </div>
              <p className=" text-white mt-[14px]">24/7 </p>
            </div>
            <div className="d" data-aos="fade-left" data-aos-duration="1700">
              <div className=" flex items-center mt-[51px]">
                <div>{/* <Image src={mail} alt="lacatsa" /> */}</div>
                <div className=" text-white ml-[10px]">
                  <h1>E-mail Us</h1>
                </div>
              </div>
              <p className=" text-white mt-[14px]">ingo@GoldenELD.com</p>
            </div>
          </div>
        </div>
        <div className=" batin-play-eps flex justify-between">
          <div
            data-aos="fade-up"
            data-aos-anchor-placement="bottom-bottom"
            data-aos-duration="1700"
          ></div>
          <div className=" mr-[170px]">
            <div data-aos="fade-right" data-aos-duration="1700">
              {/* <Image
                src={app}
                alt=""
                className=" cursor-pointer  rounded-[10px]  bg-[#858487] text-white"
              /> */}
            </div>
            <div data-aos="fade-left" data-aos-duration="1700">
              {/* <Image
                src={play}
                alt=""
                className=" cursor-pointer rounded-[10px]  mt-[24px] bg-[#858487] text-white"
              /> */}
            </div>
          </div>
        </div>
        <div
          data-aos="flip-left"
          data-aos-easing="ease-out-cubic"
          data-aos-duration="1700"
        >
          <hr className=" text-white w-[700px] h-[2px] mt-[100px] ml-[200px]" />
        </div>
        <div className="hedr-logo-icon flex justify-between mt-[51px]">
          <div
            className="hedr-logo "
            data-aos="flip-left"
            data-aos-easing="ease-out-cubic"
            data-aos-duration="1700"
          >
            {/* <Image
              src={hdlog}
              alt="hder-logo"
              className=" bg-[#1A1524] text-white"
            /> */}
          </div>
          <div className="hedr-icon bg-[#1A1524] mr-[20px]">
            <div
              className=" flex ml-[60px]"
              data-aos="fade-up-left"
              data-aos-duration="1700"
            >
              <div data-aos="fade-right" data-aos-duration="1700">
                {/* <Image src={telgrm} alt="hedir-icin" className="text-white" />
              </div>
              <div data-aos="zoom-in-down" data-aos-duration="1700">
                <Image
                  src={instg}
                  alt="hedir-icin"
                  className="text-white ml-[53px]"
                /> */}
              </div>
              <div data-aos="fade-left" data-aos-duration="1700">
                {/* <Image
                  src={fzbuk}
                  alt="hedir-icin"
                  className="text-white ml-[53px]"
                /> */}
              </div>
            </div>
            <div
              data-aos="fade-down"
              data-aos-easing="linear"
              data-aos-duration="1700"
            >
              <p className=" text-white mt-[14px]">
                © 2022 GoldenELD. All rights reserved.
              </p>
            </div>
          </div>
        </div>{" "}
        <br /> <br /> <br />
      </div>
    </div>
  );
}
