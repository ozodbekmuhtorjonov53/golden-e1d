import React from "react";
import Image from "next/image";
import { Globe, NavKorsat } from "../../../public/image";
import { HedDashed, HedSolid1, HedSolid2, HedTelefon } from "../../../public/icons";

function Header() {
  return (
    <div className="container relative pt-11 bg-[#1A1524] mx-auto flex  h-[915px] max-w-screen-2xl overflow-x-hidden">
      <div className=" flex h-[850px]">
        <Image
          className=" absolute left-[100px]"
          src={HedSolid1}
          alt="solid2"
        />
        <Image
          className=" absolute left-[755px]"
          src={HedDashed}
          alt="solid3"
        />
        <Image
          className=" absolute right-[107px]"
          src={HedSolid2}
          alt="solid2"
        />
      </div>
      <div className="px-6 md:px-[120px]">
        <h1
          className="text-white font-Inknut font-medium text-[50px] mt-14 md:mt-28 md:w-[670px] leading-[65px]"
          data-aos="fade-up"
          data-aos-duration="1500"
        >
          All in one Fleet Management Solution & ELD
        </h1>
        <p
          className="text-white font-Monserat w-[550px] text-[16px] md:text-[16px] mt-6 md:mt-12 leading-6"
          data-aos="fade-right"
          data-aos-duration="1500"
        >
          Dispatch Board will streamline day-to-day operations maximizing your
          time so you can focus on better loads and bigger profit.
          <span className="text-[#20DB81] pl-1">bigger profit.</span>
        </p>
        <div className=" flex gap-5 mt-[60px]">
          <div data-aos="fade-up-right" data-aos-duration="1500">
            <input
              className="bggrey border-solid border-[1px] border-gray-300 rounded-full py-2 px-6 w-[330px] text-white"
              type="text"
              placeholder="E-mail"
            />
          </div>
          <button
            className=" flex items-center gap-2 bg-[#6D25E1] rounded-full font-Monserat font-medium py-2 px-5 text-white"
            data-aos="fade-up-left"
            data-aos-duration="1500"
          >
            <p>Send E-mail</p>
            <Image src={NavKorsat} alt="vector" />
          </button>
        </div>
        <div className=" mt-28">
          <button
            className=" flex items-center text-white font-Monserat font-medium gap-4 bg-[#6D25E133] rounded-full border-[1px] border-solid border-[#8D65FF99] py-2 px-4"
            data-aos="fade-up"
            data-aos-duration="1500"
          >
            <Image src={HedTelefon} alt="telefxone" />
            Call Us Now
          </button>
        </div>
      </div>
      <div className="App-header  max-w-screen-2xl pb-[100px] absolute top-[-160px] left-[720px] overflow-y-hidden overflow-x-hidden">
        <Image src={Globe} className="App-logo bg-cover" alt="logo" />
      </div>
    </div>
  );
}

export default Header;
