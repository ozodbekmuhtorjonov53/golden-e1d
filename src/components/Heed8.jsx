import React from 'react'
// import { image9 } from '../../../public/image';
// import { svg30, svg31, svg32 } from '../../../public/icons';
// import Image from 'next/image';

const Heed8 = () => {
  return (
    <div className=" relative bg-[#F9F9FC] mt-[155px]">
      <div
        className="px-[24px] md:px-[120px]"
        data-aos="fade-up"
        data-aos-duration="1700"
      >
        <p className=" text-[18px] text-[#20DB81] md:pt-[60px]">Dashboard</p>
        <h1 className="text-[25px] md:text-[48px] text-[#162232] font-Inknut font-medium">
          Compliance Dashboard
        </h1>
      </div>
      <div className=" flex flex-wrap gap-[56px] px-[24px] md:pl-[60px] pb-[113px]">
        <div className=" pt-[60px]" data-aos="zoom-in" data-aos-duration="1700">
          {/* <img className=" bg-cover" src={image9} alt="image9" /> */}
          {/* <Image src={image9} alt="" /> */}
        </div>
        <div className=" pt-[60px]">
          {/* Page - 1 */}
          <div data-aos="fade-right" data-aos-duration="1700">
            {/* <Image src={svg30} alt="" /> */}
            <h1 className=" text-[18px] text-[#162232] font-Monserat font-medium pt-[13px] ">
              Current Status
            </h1>
            <p className="w-[342px] md:w-[428px] text-[#4D4B58] text-[16px] font-Monserat font-medium pt-3">
              View currents statuses and locations of your drivers. Click on a
              driver to see details.
            </p>
          </div>
          {/* Page - 2 */}
          <div
            className=" pt-[36px]"
            data-aos="fade-left"
            data-aos-duration="1700"
          >
            {/* <Image src={svg31} alt="" /> */}
            <h1 className=" text-[18px] text-[#162232] font-Monserat font-medium pt-[13px] ">
              Violations
            </h1>
            <p className="w-[342px] md:w-[428px] text-[#4D4B58] text-[16px] font-Monserat font-medium pt-3">
              Monitor violations in real time and mitigate compliance risks.
            </p>
          </div>
          {/* Page - 3 */}
          <div
            className=" pt-[36px]"
            data-aos="fade-right"
            data-aos-duration="1700"
          >
            {/* <Image src={svg32} alt="" /> */}
            <h1 className=" text-[18px] text-[#162232] font-Monserat font-medium pt-[13px] ">
              Real-time Hours
            </h1>
            <p className="w-[342px] md:w-[428px] text-[#4D4B58] text-[16px] font-Monserat font-medium pt-3">
              View real-time hours to avoid violations and regulatory ﬁnes.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Heed8