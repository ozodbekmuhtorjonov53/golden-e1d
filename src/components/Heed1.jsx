"use client";
import React from "react";
import { useEffect } from "react";
import Image from "next/image";
import AOS from "aos";
import "aos/dist/aos.css";
import { image1, image2 } from "../../public/image";

const Heed1 = () => {
  useEffect(() => {
    AOS.init({});
  });

  return (
    <div className="px-[24px] bg-[#FCFCFC] md:px-[120px]">
      <div className=" pt-[140px]">
        <div className="flex flex-wrap justify-between">
          <div>
            <p
              className=" text-[#20DB81] text-[16px] font-Monserat md:text-[18px] font-medium"
              data-aos="fade-up"
              data-aos-duration="1700"
            >
              Brief information
            </p>
            <h1
              className=" text-[#162232] text-[32px] md:text-[48px] font-Inknut font-medium pt-4"
              data-aos="fade-up"
              data-aos-duration="1700"
            >
              Golden ELD story
            </h1>
            <p
              className=" text-[#4D4B58] w-[342px] text-[18px] md:w-[477px] font-Monserat font-medium pt-9"
              data-aos="fade-right"
              data-aos-duration="1700"
            >
              The company was born in 2019 and has grown from a small group of
              people into a vast system where each employee does his job with
              high quality and goes up hard! Our mission is to makethe truckers
              life on the road comfortable and easy! And a big friendly team is
              working on developing and creating multifunctional GoldenELD
              products and features! We are grateful for your trust!
            </p>
            <button
              className=" sm:hidden bg-[#6D25E1] rounded-full py-3 px-10 font-Monserat font-medium text-[16px] mb-[31px] mt-[24px] text-white"
              data-aos="zoom-in"
              data-aos-duration="1700"
            >
              Read More{" "}
            </button>
          </div>
          <div data-aos="fade-left" data-aos-duration="1700">
            <Image className=" pt-10 bg-cover" src={image1} alt="" />
          </div>
        </div>
      </div>
      <div className=" flex flex-wrap-reverse justify-between pt-[100px]">
        <div data-aos="fade-right" data-aos-duration="1700">
          <Image className=" bg-cover" src={image2} alt="" />
        </div>
        <button
          className=" sm:hidden bg-[#6D25E1] rounded-full py-3 px-10 font-Monserat font-medium text-[16px] mb-[31px] mt-[24px] text-white"
          data-aos="zoom-in"
          data-aos-duration="1700"
        >
          Read More{" "}
        </button>
        <div data-aos="fade-left" data-aos-duration="1800">
          <h1 className=" text-[32px] text-[#162232] md:text-[48px] font-Inknut font-medium pt-4">
            Why GoldenELD ?
          </h1>
          <p className=" w-[342px] font-Monserat font-medium text-[#4D4B58] text-[18px] md:w-[470px] pt-9">
            The main goal of GoldenELD is to create a long-term partnership with
            every client. We develop new products and improve those of our
            products that are well-known on the market.
          </p>
        </div>
      </div>
    </div>
  );
};

export default Heed1;
