/* eslint-disable @next/next/no-img-element */
import React from "react";
// import Image from "next/image";

const Heed3 = () => {
  return (
    <div className="px-[24px] bg-[#F9F9FC] md:px-[119px] pt-[60px] pb-[86px]">
      <div>
        <div className="">
          <p
            className=" text-[18px] text-[#20DB81] font-Monserat font-medium"
            data-aos="fade-up"
            data-aos-duration="1700"
          >
            ELD Compliance
          </p>
          <h1
            className="pt-2 text-[32px] md:text-[48px] text-[#162232] font-Inknut font-medium"
            data-aos="fade-up"
            data-aos-duration="1700"
          >
            ELD Components
          </h1>
        </div>
        {/* 2 - page CArd */}
        <div className="flex flex-wrap justify-between pt-[64px]">
          <div data-aos="fade-down-right" data-aos-duration="1700">
            {/* <Image src={"/public/icons/svg3.svg"} alt="svg9" /> */}
            <h1 className=" pt-[13px] text-[18px] font-Monserat font-medium text-[#162232]">
              FMCSA Certified
            </h1>
            <p className="w-[342px] pt-3 md:w-[430px] text-[16px] font-medium font-Monserat text-[#4D4B58] leading-6">
              Our ELD hardware and accompaniying software were builtfoolowing
              FMCSA regulations, to help you ensure that your entire fleet is
              compliant.
            </p>
          </div>
          <div
            className="pt-10 md:p-0"
            data-aos="fade-down-left"
            data-aos-duration="1700"
          >
            {/* <Image src={svg10} alt="svg9" /> */}
            <h1 className=" pt-[13px] text-[18px] font-Monserat font-medium text-[#162232]">
              Logging Device
            </h1>
            <p className="w-[342px] pt-3 md:w-[430px] text-[16px] font-medium font-Monserat text-[#4D4B58] leading-6">
              Simply plug Electronic Logging Device into a vehicle ECM port and
              start recording driving hours and miles automatically.
            </p>
          </div>
        </div>
        {/* 3 - page CArd */}
        <div className="flex flex-wrap justify-between pt-[64px]">
          <div data-aos="fade-up-right" data-aos-duration="1700">
            {/* <Image src={svg11} alt="svg9" /> */}
            <h1 className=" pt-[13px] text-[18px] font-Monserat font-medium text-[#162232]">
              Logbook App
            </h1>
            <p className=" w-[342px] pt-3 md:w-[430px] text-[16px] font-medium font-Monserat text-[#4D4B58] leading-6">
              Logbook app connects to Electronic Logging Device via bluetooth
              and displays recorded driving time to a driver.
            </p>
          </div>
          <div
            className=" pt-10 md:p-0"
            data-aos="fade-up-left"
            data-aos-duration="1700"
          >
            {/* <Image src={svg12} alt="svg9" /> */}
            <h1 className=" pt-[13px] text-[18px] font-Monserat font-medium text-[#162232]">
              Tablet / Smartphone
            </h1>
            <p className=" w-[342px] pt-3 md:w-[430px] text-[16px] font-medium font-Monserat text-[#4D4B58] leading-6">
              ELD & App work great with most tablets and smartphones. Use your
              own or purchase devices and data plans from us.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Heed3;
