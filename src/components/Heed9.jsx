import React from "react";
import Solid03 from "./solid03";
// import { image10 } from "../../../public/image";
// import { svg13 } from "../../../public/icons";
// import Image from "next/image";

const Heed9 = () => {
  return (
    <div className="relative pt-[120px] z-10">
      <div className="bggradient relative z-10 h-[595px] md:h-[478px] px-[24px] mx-[24px]  rounded-[20px] md:mx-[100px] md:mr-[107px] md:pl-[60px] pt-[50px]">
        <div data-aos="fade-up" data-aos-duration="1500">
          <h1 className=" font-Inknut text-[#FCFCFC] text-[42px] w-[650px] ">
            A comprehensive ELD solution
          </h1>
          <p className=" pt-7 font-Monserat font-medium w-[342px] md:w-[545px] text-[18px] text-[#C1C1C6]">
            Golden ELD provides your team with a full toolkit of functionality
            to make it simple to track and manage your entire fleet.
          </p>
        </div>
        {/* page -  1  */}
        <div className=" md:pt-[50px] flex-wrap flex md:gap-[173px]">
          <div
            className=" flex items-center gap-2 md:gap-4"
            data-aos="fade-down-right"
            data-aos-duration="1700"
          >
            {/* <Image src={svg13} alt="" /> */}
            <p className=" font-Monserat text-[16px] text-[#FCFCFC]">
              HOS Logs
            </p>
          </div>
          <div
            className=" flex items-center gap-3  md:gap-4 pl-1"
            data-aos="fade-right"
            data-aos-duration="1700"
          >
            {/* <Image src={svg13} alt="" /> */}
            <p className=" font-Monserat text-[16px] text-[#FCFCFC]">
              Vehicle Inspections
            </p>
          </div>
          <div
            className=" flex items-center gap-2 md:gap-4"
            data-aos="fade-down-left"
            data-aos-duration="1700"
          >
            {/* <Image src={svg13} alt="" /> */}
            <p className=" font-Monserat text-[16px] text-[#FCFCFC]">
              IFTA Calculations
            </p>
          </div>
        </div>
        {/* page -  2  */}
        <div className="pt-[32px] flex-wrap flex md:gap-[152px]">
          <div
            className=" flex items-center gap-2 md:gap-4"
            data-aos="fade-up-right"
            data-aos-duration="1700"
          >
            {/* <Image src={svg13} alt="" /> */}
            <p className=" font-Monserat text-[16px] text-[#FCFCFC]">
              GPS Tracking
            </p>
          </div>
          <div
            className=" flex items-center gap-2 md:gap-4"
            data-aos="fade-left"
            data-aos-duration="1700"
          >
            {/* <Image src={svg13} alt="" /> */}
            <p className=" font-Monserat text-[16px] text-[#FCFCFC]">
              Trip & Route Playback
            </p>
          </div>
          <div
            className=" flex items-center gap-2 md:gap-4 pl-1"
            data-aos="fade-up-left"
            data-aos-duration="1700"
          >
            {/* <Image src={svg13} alt="" /> */}
            <p className=" font-Monserat text-[16px] text-[#FCFCFC]">
              Driver App on Android & IOS
            </p>
          </div>
        </div>
      </div>
      <div>
        {/* <Image */}
          {/* className=" bg-center bg-cover absolute -z-0 right-[108px] pt-[120px] top-0"
          src={image10}
          alt=""
        /> */}
      </div>
      <Solid03 />
    </div>
  );
};

export default Heed9;
