import React from "react";
// import { imag, img1, img5, img6, img7 } from "../../../public/image";
// import { svg1, svg2, svg3, svg4, svg5, svg6 } from "../../../public/icons";
// import Image from "next/image";

export default function Sheader() {
  return (
    <>
      <div className=" container max-w-screen-2xl px-[24px] md:px-[120px] mx-auto bg-[#1A1524]">
        <div className=" flex flex-wrap justify-between">
          <div className=" py-[140px]">
            <h1 className=" text-[white] text-[40px] md:text-[60px] uppercase">
              ELD <br />
              Configuration
            </h1>
            <p className=" text-[#C1C1C6] py-[20px] flex">
              ELDs are installed within{" "}
              <p className=" text-[#20DB81] px-2">minutes</p>
            </p>

            <button className="hidden  sm:flex justify-between items-center text-center pt-[1px] px-[15px] bg-[#8D65FF99] w-[181px] h-[50px] text-white rounded-full">
              {/* <Image src={svg5} alt="svg5" /> */}
              <p>Call Us Now</p>
            </button>
          </div>

          <div className=" flex-wrap bg-cover flex justify-between">
            {/* <Image */}
            {/* className=" hidden md:flex pt-[90px] bg-cover w-[552px] h-[452px] "
              src={imag}
              alt="imag" */}
            {/* /> */}
            {/* <Image
              className=" sm:hidden flex-wrap bg-cover"
              src={img1}
              alt="img1"
            /> */}
          </div>
        </div>
      </div>

      <div className=" container max-w-screen-2xl md:px-[120px] py-[30px] mx-auto bg-[white]">
        <div className=" text-[#20DB81] px-[24px] md:text-[20px]">
          <h1>ELD installation</h1>
        </div>
        <div className="text-[36px] px-[24px] md:text-[50px]">
          <h1>
            Istall & Connect <br />
            ELD
          </h1>
        </div>

        <div className=" flex flex-wrap justify-between  px-[24px]">
          <div>
            {/* <Image className=" pt-10" src={svg1} alt="svg1" /> */}
            <h1 className=" py-[15px]">Locate ECM (diagnostic) port</h1>
            <p className="w-[342px] md:w-[440px]">
              Locate ECM (diagnostic) port inside of your vehicle. Look for
              9-pin or 6-pin circular ports in heavy duty vehicles. Look for
              OBDII port in light/medium duty vehicles.
            </p>

            {/* <Image className=" pt-10" src={svg2} alt="svg2" /> */}
            <h1 className=" py-[15px]">Log into ELD Logbook App</h1>
            <p className="w-[342px] md:w-[440px]">
              Log in to ELD Logbook App on your tablet/smartphone with a
              username and password created during a sign up process or provided
              by a ﬂeet manager.
            </p>
          </div>

          <div>
            {/* <Image className=" pt-10" src={svg3} alt="svg3" /> */}
            <h1 className=" py-[15px]">Locate ECM (diagnostic) port</h1>
            <p className=" w-[342px] md:w-[440px]">
              Locate ECM (diagnostic) port inside of your vehicle. Look for
              9-pin or 6-pin circular ports in heavy duty vehicles. Look for
              OBDII port in light/medium duty vehicles.
            </p>

            {/* <Image className=" pt-10" src={svg4} alt="svg4" /> */}
            <h1 className=" py-[15px]">Log into ELD Logbook App</h1>
            <p className="w-[342px] md:w-[440px]">
              Log in to ELD Logbook App on your tablet/smartphone with a
              username and password created during a sign up process or provided
              by a ﬂeet manager.
            </p>
          </div>
        </div>

        {/* card - 1 */}

        <div className=" flex flex-wrap justify-between">
          <div>
            {/* <Image className=" pt-[80px] md:pt-[370px]" src={img5} alt="img5" /> */}
          </div>
          <div className=" ">
            <h1 className=" text-[#20DB81] text-[40px] pl-[60px] pt-[140px] py-[50px]">
              6 pin
            </h1>
            <div className="bg-[#E3E3E6] w-[390px] px-[25px] md:w-[620px] md:px-[56px] border-solid border-[1px] border-black bg-opacity-[0.2]">
              <div className=" flex justify-between py-5">
                <h1 className=" text-[18px] text-[ #162232] font-bold">Type</h1>
                <h1 className=" text-[18px] text-[ #162232] font-bold pl-[40px]">
                  Year
                </h1>
                <h1 className=" text-[18px] text-[ #162232] font-bold">
                  Engine
                </h1>
              </div>
              <div className="border-solid border-[1px] opacity-[0.1] border-black"></div>
              <div className=" flex justify-between">
                {/* page - 1 */}
                <div className=" leading-10 pt-[60px] pb-[60px]">
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Freightliner
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Kenworth
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Peterbilt
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    International
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    International
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Volvo
                  </p>
                </div>
                <div className=" border-solid border-[1px] border-black opacity-[0.1] my-[36px]"></div>
                {/* page - 2 */}
                <div className=" leading-10 pt-[60px] pb-[60px] text-center">
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    {" "}
                    2005 & older
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    {" "}
                    2005 & older
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    {" "}
                    2005 & older
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    {" "}
                    2006 & older
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    {" "}
                    2005 & older
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    {" "}
                    2000 & older
                  </p>
                </div>
                <div className=" border-solid border-[1px] border-black opacity-[0.1] my-[36px]"></div>
                {/* page - 3 */}
                <div className=" leading-10 pt-[60px] pb-[60px] text-end">
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    All
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    All
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    All
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Cummins
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Cat
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Volvo
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* {card2} */}

        <div className=" flex flex-wrap justify-between">
          <div>
            {/* <Image
              className=" pt-[80px md:] md:pt-[300px]"
              src={img6}
              alt="img6"
            /> */}
          </div>
          <div className=" ">
            <h1 className=" text-[#20DB81] text-[40px] pl-[60px] pt-[140px] py-[50px]">
              9 pin
            </h1>
            <div className="bg-[#E3E3E6] px-[25px] w-[390px] md:relative md:w-[620px] md:px-[56px] border-solid border-[1px] border-black bg-opacity-[0.2]">
              <div className=" flex justify-between py-5">
                <h1 className=" text-[18px] text-[ #162232] font-bold">Type</h1>
                <h1 className=" text-[18px] text-[ #162232] font-bold pl-[40px]">
                  Year
                </h1>
                <h1 className=" text-[18px] text-[ #162232] font-bold">
                  Engine
                </h1>
              </div>
              <div className="border-solid border-[1px] opacity-[0.1] border-black"></div>
              <div className=" flex justify-between">
                {/* page - 1 */}
                <div className=" leading-10 pt-[60px] pb-[60px]">
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Freightliner
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Kenworth
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Peterbilt
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    International
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    International
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Volvo
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Volvo
                  </p>
                </div>
                <div className=" border-solid border-[1px] border-black opacity-[0.1] my-[36px]"></div>
                {/* page - 2 */}
                <div className=" leading-10 pt-[60px] pb-[60px] text-center">
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    {" "}
                    2005 & older
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    {" "}
                    2005 & older
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    {" "}
                    2005 & older
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    {" "}
                    2006 & older
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    {" "}
                    2005 & older
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    {" "}
                    2000 & older
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    {" "}
                    2006-2016
                  </p>
                </div>
                <div className=" border-solid border-[1px] border-black opacity-[0.1] my-[36px]"></div>
                {/* page - 3 */}
                <div className=" text-[16px] leading-10 pt-[60px] pb-[60px] text-end">
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    All
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    All
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    All
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Cummins
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Cat
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Volvo
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Cummins
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* {card3} */}

        <div className=" flex flex-wrap justify-between gap-10">
          <div>
            {/* <Image
              className="pt-[8  0px] md:pt-[300px]"
              src={img7}
              alt="img7"
            /> */}
          </div>
          <div className=" ">
            <h1 className=" text-[#20DB81] text-[40px] pl-[60px] pt-[140px] py-[50px]">
              9 pin type 2 (green)
            </h1>
            <div className="bg-[#E3E3E6] absolute w-full md:relative md:w-[620px] px-[25px] md:px-[56px] border-solid border-[1px] border-black bg-opacity-[0.2]">
              <div className=" flex justify-between py-5">
                <h1 className=" text-[18px] text-[ #162232] font-bold">Type</h1>
                <h1 className=" text-[18px] text-[ #162232] font-bold pl-[40px]">
                  Year
                </h1>
                <h1 className=" text-[18px] text-[ #162232] font-bold">
                  Engine
                </h1>
              </div>
              <div className="border-solid border-[1px] opacity-[0.1] border-black"></div>
              <div className=" flex justify-between">
                {/* page - 1 */}
                <div className=" leading-10 pt-[60px] pb-[60px]">
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Freightliner
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Kenworth
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Peterbilt
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    International
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    International
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Volvo
                  </p>
                </div>
                <div className=" border-solid border-[1px] border-black opacity-[0.1] my-[36px]"></div>
                {/* page - 2 */}
                <div className=" leading-10 pt-[60px] pb-[60px] text-center">
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    {" "}
                    2005 & older
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    {" "}
                    2005 & older
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    {" "}
                    2005 & older
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    {" "}
                    2006 & older
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    {" "}
                    2005 & older
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    {" "}
                    2000 & older
                  </p>
                </div>
                <div className=" border-solid border-[1px] border-black opacity-[0.1] my-[36px]"></div>
                {/* page - 3 */}
                <div className=" leading-10 pt-[60px] pb-[60px] text-end">
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    All
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    All
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    All
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Cummins
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Cat
                  </p>
                  <p className="text-[16px] md:text-[18px] text-[#4D4B58] font-medium">
                    Volvo
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
