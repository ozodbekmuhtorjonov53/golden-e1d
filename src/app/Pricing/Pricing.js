import React from "react";
// import Image from "next/image";

function Pricing(props) {
  return (
    <div className="  container mx-auto pb-[174px]">
      <div className=" mx-[50px] md:pt-[150px]">
        <div className="text-center text-[36px] md:text-[72px] ">
          {" "}
          <h1>Pricing Plans</h1>
        </div>
        <div className=" justify-center flex pt-[30px]">
          {" "}
          <h1>Pricing built for businesses of</h1>{" "}
          <p className="text-[#20DB81] px-2 flex text-center">all sizes.</p>
        </div>
      </div>
      {/* {.....} */}
      <div className=" sm:px-[40px] sm:pt-[80px] md:flex flex-wrap justify-between pt-[100px] md:px-[120px] px-[20px]">
        <div className=" sm:w-[342px]  sm:h-[543px] md: bg-[#E3E3E6]  px-[25px] md:w-[420px] md:h-[632px] md:px-[56px] border-solid border-[1px] border-black bg-opacity-[0.2]">
          <div className=" md: my-9">
            <h1 className=" text-[18px] text-[ #162232] font-bold">Standard</h1>
            <p className=" py-1">Starter pack, good for Start-ups</p>
            <h1 className=" text-[39px] pt-4 flex font-bold">
              $45.00{" "}
              <span className="text-[15px] pt-6 pl-3 font-normal">
                per month
              </span>
            </h1>
          </div>
          <div className="border-solid border-[1px] opacity-[0.1] border-black"></div>
          <div className=" flex justify-between">
            {/* page - 1 */}
            <div className=" leading-10 pt-[15px] md:pb-[60px] pb-10">
              <div className=" flex gap-2">
                {/* <Image src={svg97} alt="svg7" /> */}
                <p className="text-[16px] md:text-[16px] text-[#4D4B58] font-medium">
                  Electric Logbook
                </p>
              </div>
              <div className=" flex gap-2">
                {/* <Image src={svg97} alt="svg7" /> */}
                <p className="text-[16px] md:text-[16px] text-[#4D4B58] font-medium">
                  Electronic DVIR
                </p>
              </div>
              <div className=" flex gap-2">
                {/* <Image src={svg97} alt="svg7" /> */}
                <p className="text-[16px] md:text-[16px] text-[#4D4B58] font-medium">
                  Fleet Manager Portal
                </p>
              </div>
              <div className=" flex gap-2">
                {/* <Image src={svg97} alt="svg7" /> */}
                <p className="text-[16px] md:text-[16px] text-[#4D4B58] font-medium">
                  GPS Fleet Tracking
                </p>
              </div>
              <div className=" flex gap-2">
                {/* <Image src={svg97} alt="svg7" /> */}
                <p className="text-[16px] md:text-[16px] text-[#4D4B58] font-medium">
                  IFTA reporting
                </p>
              </div>
              <div className=" py-5">
                <button className=" mt-[50px]  md:mt-[118px]  flex justify-between items-center text-center px-[15px] bg-[#6D25E1] w-[174px] h-[45px] text-white rounded-full">
                  <p>Get Started</p>
                  {/* <Image src={svg98} alt="svg8" /> */}
                </button>
              </div>
            </div>
          </div>
        </div>

        <div className=" mt-[80px] md:mt-0 sm:w-[342px] sm:h-[543px] bg-[#E3E3E6] px-[25px] md:w-[440px]  md:h-[632px] md:px-[56px] border-solid border-[1px] border-black bg-opacity-[0.2]">
          <div className=" my-6">
            <h1 className=" text-[18px] text-[ #162232] pt-5 font-bold">
              Enterprice
            </h1>
            <p className=" pt-1">Starter pack, good for Start-ups</p>
            <h1 className=" text-[40px] pt-6 font-bold">Contact US</h1>
          </div>
          <div className="border-solid border-[1px] opacity-[0.1] border-black"></div>
          <div className=" flex justify-between">
            {/* page - 2 */}
            <div className=" leading-10 pt-[15px] pb-[60px]">
              <div className=" flex gap-2">
                {/* <Image src={svg97} alt="svg7" /> */}
                <p className="text-[16px] md:text-[16px] text-[#4D4B58] font-medium">
                  Electric Logbook
                </p>
              </div>
              <div className=" flex gap-2">
                {/* <Image src={svg97} alt="svg7" /> */}
                <p className="text-[16px] md:text-[16px] text-[#4D4B58] font-medium">
                  Electronic DVIR
                </p>
              </div>
              <div className=" flex gap-2">
                {/* <Image src={svg97} alt="svg7" /> */}
                <p className="text-[16px] md:text-[16px] text-[#4D4B58] font-medium">
                  Fleet Manager Portal
                </p>
              </div>
              <div className=" flex gap-2">
                {/* <Image src={svg97} alt="svg7" /> */}
                <p className="text-[16px] md:text-[16px] text-[#4D4B58] font-medium">
                  GPS Fleet Tracking
                </p>
              </div>
              <div className=" flex gap-2">
                {/* <Image src={svg97} alt="svg7" /> */}
                <p className="text-[16px] md:text-[16px] text-[#4D4B58] font-medium">
                  IFTA reporting
                </p>
              </div>
              <div className=" flex gap-2">
                {/* <Image src={svg97} alt="svg7" /> */}
                <p className="text-[16px] md:text-[16px] text-[#4D4B58] font-medium">
                  Logbook Monitoring
                </p>
              </div>
              <div className=" flex gap-2">
                {/* <Image src={svg97} alt="svg7" /> */}
                <p className="text-[16px] md:text-[16px] text-[#4D4B58] font-medium">
                  24/7 support
                </p>
              </div>

              <div className=" sm: mt-[52px] md:mt-[52px]">
                <button className=" flex justify-between items-center text-center pt-[1px] px-[15px] bg-[#6D25E1] w-[174px] h-[45px] text-white rounded-full">
                  <p>Get Started</p>
                  {/* <Image src={svg98} alt="svg8" /> */}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Pricing;
