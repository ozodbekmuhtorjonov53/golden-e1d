import React from "react";
import Header from "../../src/components/header/Header";
import Hedd1 from "../components/Heed1";
import Heed2 from "../components/Heed2";
import Heed3 from "../components/Heed3";
import Heed4 from "../components/Heed4";
import Heed5 from "../components/Heed5";
import Heed6 from "../components/Heed6";
import Heed7 from "../components/Heed7";
import Heed8 from "../components/Heed8";
import Heed9 from "../components/Heed9";
import Heed10 from "../components/Heed10";
import Heed11 from "../components/Heed11";
// import Solid from "../components/Solid";

export default function HeaderPage() {
  return (
    <div className="container mx-auto max-w-screen-2xl">
      <div className="relative">
        <Header />
      </div>
      <div className="relative">
        <Hedd1 />
        <Heed2 />
        <Heed3 />
        <Heed4 />
        <Heed5 />
        <Heed6 />
        <Heed7 />
        <Heed8 />
        <Heed9 />
        <Heed10 />
        <Heed11 />
        {/* <Solid/> */}
      </div>
    </div>
  );
}
